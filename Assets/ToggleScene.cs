﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ToggleScene : MonoBehaviour {

    public int scene;
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Restart"))
        {
            SceneManager.LoadScene(scene);
        }
	}
}

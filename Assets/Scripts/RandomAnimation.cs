﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAnimation : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Animator>().SetFloat("Offset", Random.value);
        GetComponent<Animator>().SetFloat("Speed", Random.Range(0.5f, 1.5f));
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLerp : MonoBehaviour {

    public Transform target;

    public float speed;
	public Vector3 CameraPosition;

	public bool CameraShake;
	public float CameraShakeMagnitude;


	void Start () {
		ImpactHandler h = FindObjectOfType<ImpactHandler> ();
		h.ImpactUpdate.AddListener( (bool shake) => {CameraShake = shake;} );
		CameraPosition = transform.position;
	}


	void Update () {
        Vector3 goalPosition = target.position;
		goalPosition.y = CameraPosition.y;
		goalPosition.z = CameraPosition.z;
		CameraPosition = Vector3.Lerp(CameraPosition, goalPosition, speed * Time.deltaTime);

		transform.position = CameraPosition;
		if (CameraShake) {
			transform.position += (Vector3)Random.insideUnitCircle *  CameraShakeMagnitude;
		}

	}

	public void SetCameraShake(bool shake) {
	}
		
}

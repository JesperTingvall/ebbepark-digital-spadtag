﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicInput : MonoBehaviour {

    private ReadSettings settings;

    public static float MicLoudness;

    private string _device;

    public bool KeyboardInput = false;
    public float KeyboardInputStrength = 10;
    
	public void ListDevices() {
		Debug.Log ("Microphone devices:");
		foreach (var mic in Microphone.devices)
			Debug.Log (" - " + mic);
	}

    //mic initialization
    void InitMic() {
		if (_device == null) {
			ListDevices ();
			if (settings.settings.MicDevice < Microphone.devices.Length && settings.settings.MicDevice >= 0) {
				_device = Microphone.devices [settings.settings.MicDevice];
				Debug.Log ("Using microphone device " + settings.settings.MicDevice + ": " + Microphone.devices [settings.settings.MicDevice]);
			} else
				Debug.LogError ("Can not find mic device " + settings.settings.MicDevice);
		}
		_clipRecord = Microphone.Start(_device, true, 999, 44100);
    }

    void StopMicrophone() {
        Microphone.End(_device);
    }


    AudioClip _clipRecord = new AudioClip();
    int _sampleWindow = 128;

    //get data from microphone into audioclip
    float LevelMax() {
        float levelMax = 0;
        float[] waveData = new float[_sampleWindow];
        int micPosition = Microphone.GetPosition(null) - (_sampleWindow + 1); // null means the first microphone
        if (micPosition < 0)
            return 0;
        _clipRecord.GetData(waveData, micPosition);
        // Getting a peak on the last 128 samples
        for (int i = 0; i < _sampleWindow; i++) {
            float wavePeak = waveData[i] * waveData[i];
            if (levelMax < wavePeak) {
                levelMax = wavePeak;
            }
        }
        return levelMax;
    }

    void Update()
    {
        // levelMax equals to the highest normalized value power 2, a small number because < 1
        // pass the value to a static var so we can access it from anywhere
		MicLoudness = 0;
        if (KeyboardInput)
        {
            MicLoudness += KeyboardInputStrength * Mathf.Abs( Input.GetAxis("Vertical"));
        }
        MicLoudness += LevelMax() * settings.settings.MicScale;
        
    }

    bool _isInitialized;
    // start mic when scene starts
    void OnEnable() {
        settings = FindObjectOfType<ReadSettings>();
        InitMic();
        _isInitialized = true;
    }

    //stop mic when loading a new level or quit application
    void OnDisable() {
        StopMicrophone();
    }

    void OnDestroy() {
        StopMicrophone();
    }


    // make sure the mic gets started & stopped when application gets focused
    void OnApplicationFocus(bool focus) {
        if (focus) {
            //Debug.Log("Focus");

            if (!_isInitialized) {
                //Debug.Log("Init Mic");
                InitMic();
                _isInitialized = true;
            }
        }
        if (!focus) {
            //Debug.Log("Pause");
            StopMicrophone();
            //Debug.Log("Stop Mic");
            _isInitialized = false;

        }
    }
}

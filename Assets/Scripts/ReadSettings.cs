﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.Serialization;
using UnityEngine;

public class ReadSettings : MonoBehaviour
{
    public string SettingsFile;

    [System.Serializable]
    public class SettingsData
    {
        public float MicScale;
		public int MicDevice;

        public SettingsData()
        {
			MicDevice = 0;
            MicScale = 50;
        }
    }

    public SettingsData settings;

    private SettingsData LoadSettings()
    {
        // Path.Combine combines strings into a file path
        // Application.StreamingAssets points to Assets/StreamingAssets in the Editor, and the StreamingAssets folder in a build
        string filePath = Path.Combine(Application.streamingAssetsPath, SettingsFile);
        Debug.Log(filePath);

        if (File.Exists(filePath))
        {
            // Read the json from the file into a string
            string dataAsJson = File.ReadAllText(filePath);
            // Pass the json to JsonUtility, and tell it to create a GameData object from it
            SettingsData loadedData = JsonUtility.FromJson<SettingsData>(dataAsJson);
            return loadedData;    
        }
        else
        {
            Debug.LogError("Failed to load " + filePath);
            return new SettingsData();
        }
    }

    // Use this for initialization
    void OnEnable() {
        settings = LoadSettings();

		//Debug.Log (JsonUtility.ToJson (new SettingsData ()));
        
	}
	
}

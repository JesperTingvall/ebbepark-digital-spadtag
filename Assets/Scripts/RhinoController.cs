﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RhinoController : MonoBehaviour {
    public float Scale;
    public float LerpSpeed;

    public float MaxY, MinY;

    public float GravityVelocity;
    public bool Alive = true;

    void Update() {
        float v = Scale * MicInput.MicLoudness;
        float p = Mathf.Clamp(transform.position.y + (v + GravityVelocity) * Time.deltaTime, MinY, MaxY);
        transform.position = new Vector3(transform.position.x, p, transform.position.z);
    }

    public void OnEnable() {
        Alive = true;
        GetComponent<Animator>().SetBool("Alive", true);
    }

    public void Death() {
        if (Alive) {
            GetComponent<Animator>().SetBool("Alive", false);
            Alive = false;
        }
    }
}
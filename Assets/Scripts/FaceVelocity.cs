﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceVelocity : MonoBehaviour {

    private Rigidbody2D body;

    public float LerpSpeed;
    

	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody2D>();
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        float angle = Mathf.Atan2( body.velocity.y, body.velocity.x) * Mathf.Rad2Deg;

        if (angle > 90)
            angle -= 180;

        if (angle < -90)
            angle += 180;

        

        body.MoveRotation(Mathf.LerpAngle(transform.rotation.eulerAngles.z, angle, LerpSpeed * Time.fixedDeltaTime));
	}
}

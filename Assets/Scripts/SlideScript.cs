﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideScript : MonoBehaviour {
    public SlideScript NextSlide;

    [HideInInspector]
    public SlideScript PreviousSlide;

	void Start () {
        if (NextSlide)
            NextSlide.PreviousSlide = this;
	}
	
	void Update () {
        if (Input.GetButtonDown("Next") && NextSlide) {
            NextSlide.SwitchToSlide();
            SwitchFromSlide();
        }
        if (Input.GetButtonDown("Previous") && PreviousSlide) {
            PreviousSlide.SwitchToSlide();
            SwitchFromSlide();
        }

		
	}

    public void SwitchFromSlide() {
        gameObject.SetActive(false);
    }

    public void SwitchToSlide() {
        gameObject.SetActive(true);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private Rigidbody2D body;
    
    public float MaxVelocity;
    public float ForwardSpeed;

   // public 

	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        float Strength = MicInput.MicLoudness;

        Vector2 velocity = body.velocity;
        velocity.y = (Strength - 3);
        velocity.x = ForwardSpeed;
        velocity = Vector2.ClampMagnitude(velocity, MaxVelocity);
        body.velocity = velocity;

        //body.AddForce(Vector2.right * ForwardSpeed * Time.fixedDeltaTime, ForceMode2D.Force); // * Time.fixedDeltaTime;
        
        
       // float v = Scale * MicInput.MicLoudness;
      //  float p = Mathf.Clamp(transform.position.y + (v + GravityVelocity) * Time.deltaTime, MinY, MaxY);
      //  transform.position = new Vector3(transform.position.x, p, transform.position.z);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class MyIntEvent : UnityEvent<bool>
{
}

public class ImpactHandler : MonoBehaviour {
	public UnityEvent<bool> ImpactUpdate = new MyIntEvent();

	public ParticleSystem Dust;

	private int Colliders;

	void OnCollisionEnter2D(Collision2D coll) {
		Colliders++;
		ImpactUpdate.Invoke (Colliders > 0);
	}

	void OnCollisionExit2D(Collision2D coll) {
		Colliders=0;
		ImpactUpdate.Invoke (Colliders > 0);
	}

    public void OnCollisionStay2D(Collision2D collision)
	{
		if (collision.collider.tag != "NoDust") {
			Dust.Emit (1);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableOtherObject : MonoBehaviour {
    public GameObject other;

	void Start () {
        other.SetActive(gameObject.activeInHierarchy);
	}

    private void OnEnable() {
        other.SetActive(true);
    }

    private void OnDisable() {
        other.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeScaler : MonoBehaviour {

    public float Scale;
    public float LerpSpeed;

    private float phase;
    private float frequency;

    private Vector3 startPosition;

	private float s;

	void Start () {
        startPosition = transform.position;
        phase = Random.Range(0, 10);
        frequency = Random.Range(5f, 10);
	}
	
	void Update () {
		s = Mathf.Lerp(s, Scale/2.0f * MicInput.MicLoudness, LerpSpeed * Time.deltaTime);

		transform.position = startPosition + s * Vector3.up * Mathf.Sin(frequency * Time.time + phase);

        //transform.localScale = new Vector3(1, s, 1);
	}
}
